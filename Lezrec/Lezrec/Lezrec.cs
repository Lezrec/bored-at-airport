﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lezrec {
    namespace Debug {

        //Allows you to create different types of debugs
        public partial class Debug : Attribute {
            public readonly string Location;
            public Debug(string Location) {
                this.Location = Location;
            }

            new public Type GetType() {
                return base.GetType();
            }

            public override string ToString() {
                return $"BASE DEBUG FROM {Location}";
            }
        }
        [Debug(
            Location: 
            "DisposedDebugStackException")]
        public class DisposedDebugStackException : Exception {
            private readonly string info;
            private readonly DebugStack stack;
            public readonly Debug Debug;
            public DisposedDebugStackException(string info, DebugStack stack) {
                this.info = info;
                this.stack = stack;                
                Debug = (Debug)Attribute.GetCustomAttribute(typeof (DisposedDebugStackException) ,typeof (Debug));
            }

            public string Information {
                get { return info != null ? info : "No information provided"; }
            }

            public override string ToString() {
                return $"{base.ToString()} :: {Information} :: from stack {stack.ToString()}";
            }

            public DebugStack Stack {
                get { return stack != null ? stack : throw new NullReferenceException("No DebugStack attached to this Exception"); }
            }
        }
        [Debug(
            Location:
            "DebugQuery")]
        public class DebugQuery {
            public enum QueryPriority {
                Assertion,
                Notification,
                Alert,
                Warning,
                Error
            }
            public readonly string Information;
            public readonly QueryPriority Priority;
            public readonly Debug Debug;
            
            public DebugQuery(string Information, QueryPriority Priority) {
                this.Information = Information;
                this.Priority = Priority;
                Debug = (Debug)Attribute.GetCustomAttribute(typeof(DebugQuery), typeof(Debug));
            }

            public override string ToString() {
                return $"Base DebugQuery containing Information: {Information} and Priority: {Priority.ToString()} :: DEBUG: {Debug.ToString()}";
            }

        }
        [Debug(
            Location:
            "DebugStack")]
        [Serializable]
        public class DebugStack : Stack<DebugQuery>, IDisposable {
            private static List<string> IDS = new List<string>();
            private bool active;
            private uint size;
            public readonly string ID;
            public DebugStack() {
                active = true;
                size = 0;
                Random rand = new Random();
                //now if you have like a bazillion stacks that might be a problem
                string tempID = $"{rand.Next(int.MaxValue)}";
                while(IDS.Contains(tempID)) tempID = $"{rand.Next(int.MaxValue)}";
                ID = tempID;    
            }

            private void K() {
                if (!active) throw new DisposedDebugStackException(
                    info:
                    "The stack was disposed and is currently not available for use.",
                    stack:
                    this);
            }
            //for when you want to dispose but keep the stack alive :D
            public Stack<DebugQuery> Puke() {
                var ret = new Stack<DebugQuery>();
                foreach(DebugQuery query in this.ToArray()) {
                    ret.Push(query);
                    Pop();
                }
                size = 0; //just for fun :P
                return ret;
            }


            new public void Push(DebugQuery query) {
                K();
                base.Push(query);
                ++size;
            }
            
            new public DebugQuery Peek() {
                K();
                //size not changing
                return base.Peek();
            }

            new public DebugQuery Pop() {
                K();
                --size;
                return base.Pop();
            }

            public void Dispose() {
                K();
                Clear();
                active = false;
            }

            public uint Size {
                get { return size; }
            }

            public override string ToString() {
                return $"Stack containing {size} Elements :: ID = {ID}";
            }
        }
    }
}
