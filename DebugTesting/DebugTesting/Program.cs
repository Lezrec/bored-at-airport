﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lezrec.Debug;

namespace DebugTesting {
    class Program {
        static void Main(string[] args) {
            DebugStack stack = new DebugStack();
            string str = Console.In.ReadLine();
            stack.Push(new DebugQuery(
                    Information: str,
                    Priority: DebugQuery.QueryPriority.Assertion
                    ));
            while (str != "!") {
                str = Console.In.ReadLine();
                stack.Push(new DebugQuery(
                    Information: str,
                    Priority: DebugQuery.QueryPriority.Assertion
                    ));
            }
            while(stack.Size > 0) {
                Console.WriteLine(stack.Pop());
            }
            Console.Read();
        }
    }
}
